package com.gitlab.lgcavalheiro.submarinecontrols.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class TripControllerTests {
        @Autowired
        private TripController controller;

        @Test
        void postCommandShouldReturnCommandResult() throws Exception {
                String result = controller.postCommand("RMMLMMMDDLL");

                assertThat(result).isEqualTo("2 3 -2 SUL");
        }
}
