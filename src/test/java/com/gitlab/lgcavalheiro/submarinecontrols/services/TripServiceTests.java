package com.gitlab.lgcavalheiro.submarinecontrols.services;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(Lifecycle.PER_CLASS)
public class TripServiceTests {
    private TripService service;

    @BeforeAll
    void setup() {
        this.service = new TripService();
    }

    @Test
    void canExecuteTrip() {
        String result = this.service.executeTrip("RMMLMMMDDLL");

        assertThat(result).isEqualTo("2 3 -2 SUL");
    }
}
