package com.gitlab.lgcavalheiro.submarinecontrols.models;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

@TestInstance(Lifecycle.PER_CLASS)
public class TripTests {
    HashMap<String, Integer> defaultPosition = new HashMap<String, Integer>();
    Integer defaultDirection;

    HashMap<String, Integer> userPosition = new HashMap<String, Integer>();
    Integer userDirection;
    String command;

    @BeforeAll
    void setup() {
        this.defaultPosition.put("x", 0);
        this.defaultPosition.put("y", 0);
        this.defaultPosition.put("z", 0);
        this.defaultDirection = 1;

        this.userPosition.put("x", 3);
        this.userPosition.put("y", -2);
        this.userPosition.put("z", -5);
        this.userDirection = 4;
        this.command = "UDMLR";
    }

    @Test
    void canCreateWithJustCommand() {
        Trip trip = new Trip(this.command);

        assertThat(trip.getCommand()).isEqualTo(this.command);
        assertThat(trip.getDirection()).isEqualTo(this.defaultDirection);
        assertThat(trip.getPosition()).isEqualTo(this.defaultPosition);
    }

    @Test
    void canCreateWithCommandAndDirection() {
        Trip trip = new Trip(this.command, this.userDirection);

        assertThat(trip.getCommand()).isEqualTo(this.command);
        assertThat(trip.getDirection()).isEqualTo(this.userDirection);
        assertThat(trip.getPosition()).isEqualTo(this.defaultPosition);
    }

    @Test
    void canCreateWithCommandAndPosition() {
        Trip trip = new Trip(this.command, this.userPosition);

        assertThat(trip.getCommand()).isEqualTo(this.command);
        assertThat(trip.getDirection()).isEqualTo(this.defaultDirection);
        assertThat(trip.getPosition()).isEqualTo(this.userPosition);
    }

    @Test
    void canCreateWithAllParameters() {
        Trip trip = new Trip(this.command, this.userPosition, this.userDirection);

        assertThat(trip.getCommand()).isEqualTo(this.command);
        assertThat(trip.getDirection()).isEqualTo(this.userDirection);
        assertThat(trip.getPosition()).isEqualTo(this.userPosition);
    }

    @Test
    void canToString() {
        Trip trip = new Trip(this.command);

        assertThat(trip.toString()).isEqualTo("0 0 0 NORTE");
    }

    @Test
    void throwsIfDirectionIsInvalid() {
        Trip trip = new Trip(this.command, 6);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            trip.getStringDirection();
        });

        assertThat(exception.getMessage()).isEqualTo("Integer directions must be between 1 and 4");
    }

    @ParameterizedTest
    @CsvFileSource(resources = "./data.csv", numLinesToSkip = 1)
    void canExecuteTripCorrectly(String input, String expected) {
        Trip trip = new Trip(input);

        assertThat(trip.execute()).isEqualTo(expected);
    }
}
