package com.gitlab.lgcavalheiro.submarinecontrols.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.lgcavalheiro.submarinecontrols.services.TripService;

@RestController
public class TripController {
    private final TripService tripService;

    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PostMapping("/api/trip/execute")
    public String postCommand(@RequestBody String command) {
        return this.tripService.executeTrip(command);
    }
}