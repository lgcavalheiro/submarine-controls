package com.gitlab.lgcavalheiro.submarinecontrols.services;

import com.gitlab.lgcavalheiro.submarinecontrols.models.Trip;

import org.springframework.stereotype.Service;

@Service
public class TripService {
    public String executeTrip(String command) {
        Trip trip = new Trip(command);

        return trip.execute();
    }
}
