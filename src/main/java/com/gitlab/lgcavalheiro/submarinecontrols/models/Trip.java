package com.gitlab.lgcavalheiro.submarinecontrols.models;

import java.util.HashMap;
import java.util.Map;

public class Trip {
    enum DIRECTIONS {
        NORTE,
        SUL,
        LESTE,
        OESTE
    }

    enum STEPS {
        L,
        R,
        M,
        U,
        D
    }

    private String command;
    private Map<String, Integer> position;
    private Integer direction;

    public Trip(String command, Map<String, Integer> position, Integer direction) {
        this.command = command.toUpperCase();
        this.position = position;
        this.direction = direction;
    }

    public Trip(String command, Integer direction) {
        this.position = new HashMap<String, Integer>();
        this.position.put("x", 0);
        this.position.put("y", 0);
        this.position.put("z", 0);

        this.command = command.toUpperCase();
        this.direction = direction;
    }

    public Trip(String command, Map<String, Integer> position) {
        this.command = command.toUpperCase();
        this.position = position;
        this.direction = 1;
    }

    public Trip(String command) {
        this.position = new HashMap<String, Integer>();
        this.position.put("x", 0);
        this.position.put("y", 0);
        this.position.put("z", 0);
        this.direction = 1;

        this.command = command.toUpperCase();
    }

    public String toString() {
        return this.position.get("x") + " " + this.position.get("y") + " " + this.position.get("z") + " "
                + this.getStringDirection();
    }

    private Integer resolveMoveDirection(String axis) {
        if (axis == "y")
            return (this.direction + 3) % 4 == 0 ? 1 : -1;

        return (this.direction + 4) % 6 == 0 ? -1 : 1;
    }

    public String execute() {
        for (String step : this.command.split("")) {
            STEPS validStep;

            try {
                validStep = STEPS.valueOf(step);
            } catch (IllegalArgumentException e) {
                continue;
            }

            switch (validStep) {
                case D:
                    this.position.put("z", this.position.get("z") - 1);
                    break;
                case U:
                    if (this.position.get("z") < 0)
                        this.position.put("z", this.position.get("z") + 1);
                    break;
                case L:
                    if (this.direction == 4)
                        this.direction = 1;
                    else
                        this.direction++;
                    break;
                case R:
                    if (this.direction == 1)
                        this.direction = 4;
                    else
                        this.direction--;
                    break;
                case M:
                    if (this.direction % 2 > 0)
                        this.position.put("y", this.position.get("y") + this.resolveMoveDirection("y"));
                    else
                        this.position.put("x", this.position.get("x") + this.resolveMoveDirection("x"));
                    break;
                default:
                    break;
            }
        }

        return this.toString();
    }

    public String getCommand() {
        return command;
    }

    public Map<String, Integer> getPosition() {
        return position;
    }

    public Integer getDirection() {
        return direction;
    }

    public DIRECTIONS getStringDirection() {
        switch (this.direction) {
            case 1:
                return DIRECTIONS.NORTE;
            case 2:
                return DIRECTIONS.OESTE;
            case 3:
                return DIRECTIONS.SUL;
            case 4:
                return DIRECTIONS.LESTE;
            default:
                throw new IllegalArgumentException("Integer directions must be between 1 and 4");
        }
    }

}
