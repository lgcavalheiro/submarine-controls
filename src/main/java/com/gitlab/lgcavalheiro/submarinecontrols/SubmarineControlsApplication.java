package com.gitlab.lgcavalheiro.submarinecontrols;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubmarineControlsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubmarineControlsApplication.class, args);
	}

}
